import React from "react";
import './Boton.css';

function Boton(props) {
    const onClickBoton = (msg) => {
        alert(msg);
    }

    return(
        <button className="Boton"
        onClick={() => onClickBoton('Abrir modal')}
        >
            +
        </button>
    );
}

export {Boton};