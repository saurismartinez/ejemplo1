import './App.css';

import React from 'react' 
//import './App.css';
import {Contador} from './Contador.js'
import {Buscador} from './Buscador.js'
import {Lista} from './Lista.js'
import {Boton} from './Boton.js'
import {Item} from './Item.js'


const defaultTareas = [
  {text: 'Inicializar proyecto React', completed:false},
  {text: 'Crear componentes', completed: false},
  {text: 'CSS en React', completed: false},
  {text: 'Eventos con React', completed: false},
];


function App() {

  const [tareas, setTareas] = React.useState(defaultTareas);
  const [searchValue, setSearchValue] = React.useState('');

  const completedTareas = tareas.filter(tarea => !!tarea.completed).length;
  const totalTareas = tareas.length;

  let searchedTareas = [];

  if(!searchValue.length >= 1) {
    searchedTareas = tareas;
  } else {
    searchedTareas = tareas.filter(tarea => {
      const tareaText = tarea.text.toLowerCase();
      const searchText = searchValue.toLowerCase();
      return tareaText.includes(searchText);
    })
  }


  //funcion para marcar la tarea como completada
  const completeTarea = (text) => {
    const tareaIndex = tareas.findIndex(tarea => tarea.text === text);
    const newTareas = [...tareas];
    newTareas[tareaIndex].completed = true;
    setTareas(newTareas);
  };

  //funcion para eliminar la tarea
  const deleteTarea = (text) => {
    const tareaIndex = tareas.findIndex(tarea => tarea.text === text);
    const newTareas = [...tareas];
    newTareas.splice(tareaIndex, 1);
    setTareas(newTareas);
  };

  return (
    <React.Fragment>
 <Contador 
            total={totalTareas}
            completed={completedTareas}
          />
          <Buscador 
            searchValue = {searchValue}
            setSearchValue = {setSearchValue}
          />
          <Lista>
            {searchedTareas.map(tarea => (
              <Item 
                key={tarea.text} 
                text={tarea.text}
                completed={tarea.completed}
                onComplete={() => completeTarea(tarea.text)}
                onDelete={() => deleteTarea(tarea.text)}
            />))}
          </Lista>
        <Boton />
   
 
  </React.Fragment>
  );
}

export default App;
